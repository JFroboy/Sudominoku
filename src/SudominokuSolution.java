package src;
import java.util.ArrayList;
import java.util.Random;


/**
 *
 * Jhon Edinson Blandon Quintero – 1255414
 * Sebastian Cano Ruiz – 1556156
 * Juan Manuel Sanchez Villada   – 1556275
 */
public class SudominokuSolution
{
    /**
     * Para dar solucion mediante algoritmo de amplitud, cada nodo del arbol es objeto de tipo ficha (tablero, bolsa_fichas)
     * en cada iteracion se expande el primer nodo del arbol y despues de ser expandido es eliminado.
     * El algoritmo de Vegas aplica aletoriedad al momento de decidir que nodo del arbol expandir
     * @param tablero_inicial tablero de juego
     * @param bolsa_inicial lista de fichas disponibles
     * @param algoritmoVegas boolean que determina cual algoritmo se desea ejecturas Vegas o Amplitud
     * @return
     */
    public boolean algoritmoAmplitud_And_Vegas1(Integer[][] tablero_inicial, ArrayList<int[]> bolsa_inicial, boolean algoritmoVegas)
    {
        /** Se almacena el nodo raiz*/
        Jugada jugada = new Jugada(tablero_inicial, bolsa_inicial);
        ArrayList<Jugada> arbol = new ArrayList<>();
        arbol.add(jugada);
        int fila;
        int columna;
        int[] celda;
        int num_nodo = 0;
        int numIteraciones = 0;

        while (true)
        {
            if (algoritmoVegas)
            {
                Random aleatorio = new Random(System.currentTimeMillis());
                num_nodo = aleatorio.nextInt(arbol.size());
            }

            else
            {
                num_nodo = 0;
            }


            /** Se crea un tablero y bolsa que serán una copia del nodo a ser expandido*/
            Integer[][] tableroNodo = new Integer[9][9];
            ArrayList<int[]> bolsaNodo = new ArrayList<>();

            copiar_tablero(arbol.get(num_nodo).getTablero(), tableroNodo);
            copiar_bolsa(arbol.get(num_nodo).getBolsa(), bolsaNodo);
            celda = celdaDisponible(tableroNodo);
            fila = celda[0];
            columna = celda[1];

            /** Si el arreglo de celda, contiene valores -1 indica que el tablero se lleno*/
            if (columna == -1)
            {
            	copiar_tablero(tableroNodo, tablero_inicial);
                break;
           	}

            /** Se obtiene las fichas factibles para ser insertadas en el tablero */
            ArrayList<int[]> posibles = posibles(tableroNodo, bolsaNodo, fila, columna);

            /** Si el Arraylist de posibles no tiene fichas, indica que se llegó a una solucion NO factible */
            if (posibles.isEmpty())
            {
                arbol.remove(num_nodo);
            }
            else
            {
                /** Se toma cada ficha posible es incluida en el tablero y descontada de la bolsa
                 * se procede a realizar la recurrsion, para hallar la siguiente celda disponible y encontrar las fichas
                 * factibles en esa celda
                 * En cada iteracion del for, el tablero y bolsa de fichas son restauradas a la del nodo que se esta
                 * expandiendo. Esto se hace por que cada nodo hijo es independiente a otro*/

                for (int i = 0; i < posibles.size(); i++)
                {
                    incluirFicha(tableroNodo, posibles.get(i), fila, columna);
                    eliminarFichaBolsa(bolsaNodo, posibles.get(i));
                    Jugada posibleWay = new Jugada(tableroNodo, bolsaNodo);
                    arbol.add(posibleWay);

                    tableroNodo = new Integer[9][9];
                    copiar_tablero(arbol.get(num_nodo).getTablero(), tableroNodo);
                    bolsaNodo = new ArrayList<>();
                    copiar_bolsa(arbol.get(num_nodo).getBolsa(), bolsaNodo);
                }
                arbol.remove(num_nodo);
            }
            numIteraciones ++;
            System.out.println("Iteracion Numero: " + numIteraciones);
        }
        return true;
    }

    public  void copiar_tablero(Integer[][] tableroOrigen, Integer[][] tableroDestino)
    {
        for (int i = 0; i <9 ; i++) {
            for (int j = 0; j <9 ; j++) {
                tableroDestino[i][j] = tableroOrigen[i][j];
            }

        }
    }

    public  void copiar_bolsa (ArrayList<int[]> bolsaOrigen, ArrayList<int[]> bolsaDestino)
    {
        for (int i = 0; i < bolsaOrigen.size(); i++) {
            int[] ficha = {bolsaOrigen.get(i)[0], bolsaOrigen.get(i)[1]};
            bolsaDestino.add(ficha);
        }
    }

    /**
     * Este metodo consiste en tomar fichas aleatoriamente de las posibles para una determinada celda
     * e ir ubicando en el tablero
     * @param tablero_inicial tablero de juego
     * @param bolsa_inicial lista de fichas disponibles
     * @return res int[] la posicion 0 indica si se soluciono o no el tablero (0 true | 1 false) y la posicion 1 almacena
     * el total de fichas ubicadas en el tablero.
     */
    public int[] algoritoVegas2(Integer[][] tablero_inicial, ArrayList<int[]> bolsa_inicial)
    {
        int res[] = {1,0};
        Integer[][] tablero = new Integer[9][9];
        copiar_tablero(tablero_inicial, tablero);
        ArrayList<int[]> bolsa = new ArrayList<>();
        copiar_bolsa(bolsa_inicial, bolsa);
        int fila;
        int columna;
        int[] celda;
        Random aleatorio = new Random(System.currentTimeMillis());
        int intAletorio;

        while (true)
        {
            celda = celdaDisponible(tablero);
            fila = celda[0];
            columna = celda[1];

            /** Si el arreglo de celda, contiene valores -1 indica que el tablero se lleno*/
            if (columna == -1)
            {
                res[0] = 0;
                copiar_tablero(tablero, tablero_inicial);
                break;
            }

            /** Se obtiene las fichas factibles para ser insertadas en el tablero */
            ArrayList<int[]> posibles = posibles(tablero, bolsa, fila, columna);

            /** Si el Arraylist de posibles no tiene fichas, indica que se llegó a una solucion NO factible */
            if (posibles.isEmpty())
            {
                break;
            }

            else
            {
                /** Se genera una posicion aleatoriamente de la ficha que sera ubicada en el tablero */
                intAletorio = aleatorio.nextInt(posibles.size());

                /** Se toma una ficha aleatoriamente de la lista de posible y es incluida en el tablero posteriormente
                 * es descontada de la bolsa */
                incluirFicha(tablero, posibles.get(intAletorio), fila, columna);
                eliminarFichaBolsa(bolsa, posibles.get(intAletorio));
                /** Se lleva una conteo de las fichas que son ubicadas cada que se ejecuta la funcion */
                res[1] += 1;
            }
            aleatorio.setSeed(System.currentTimeMillis());
        }
        return res;
    }

    /**
     * Dada un x ficha se elimina de la bolsa
     * @param bolsa bolsa con las fichas disponibles
     * @param ficha ficha
     */
    public void eliminarFichaBolsa(ArrayList<int[]> bolsa, int[] ficha)
    {
        int cara1 = ficha[0];
        int cara2 = ficha[1];
        for (int i = 0; i <bolsa.size() ; i++) {
            if((bolsa.get(i)[0] == cara1) && (bolsa.get(i)[1] == cara2))
            {
                bolsa.remove(i);
            }
        }
    }

    /**
     * Dada las coordenadas de una celda y una x ficha esta se prodece a ser insertada en el tablero
     * @param tablero tablero del juego
     * @param ficha ficha[cara1,cara2,rotacion] para ser incluida en el tablero
     * @param fila fila que contiene la ficha a insertar
     * @param columna columna que contiene la ficha a insertar
     */
    public void incluirFicha(Integer[][] tablero, int[] ficha,int fila, int columna) {
        int cara1 = ficha[0];
        int cara2 = ficha[1];
        int rotacion = ficha[2];

        if(rotacion == 0) {
            tablero[fila][columna] = cara1;
            tablero[fila + 1][columna] = cara2;
        }

        if(rotacion == 90) {
            tablero[fila][columna + 1] = cara1;
            tablero[fila][columna] = cara2;
        }
        if(rotacion == 180) {
            tablero[fila + 1][columna] = cara1;
            tablero[fila][columna] = cara2;
        }

        if(rotacion == 270) {
            tablero[fila][columna] = cara1;
            tablero[fila][columna + 1] = cara2;
        }


    }

    /**
     * Se va recorriendo el tablero, para determinar si hay o no una celda vacia ("0")
     * @param tablero tablero del juego
     * @return int[] un arreglo que tendra las coordenadas(fila,columna) de una celda disponible del tablero
     * si no encuentra ninguna celda, indica que el tablero se resolvio y retornara -1 en ambas posiciones para
     * ser validado en la función principal
     */
    public int[] celdaDisponible(Integer[][] tablero)
    {
        int[] celda = new int[2];

        for (int i = 0; i <9 ; i++) {
            for (int j = 0; j <9 ; j++) {
                if(tablero[i][j] == 0)
                {
                    celda[0] = i;
                    celda[1] = j;
                    return celda;
                }
            }

        }

        celda[0] = -1;
        celda[1] = -1;
        return celda;
    }

    /**
     * Se van tomando fichas de la bolsa para determinar cuales son validas para ser ubicadas en la celda dada
     * se tiene en cuenta las rotaciones es decir una misma ficha puede ubicarse de 4 maneras en este caso, se almacenara
     * la ficha con su respectiva rotacion {ficha1,rotacion 0 - ficha1,rotacion 90 - ficha1,rotacion 180 - ficha1,rotacion 270}
     * @param tablero tablero del juego
     * @param bolsa bolsa con las fichas disponibles para ubicar
     * @param fila fila disponible para ubicar un x ficha
     * @param columna columna disponible para ubicar un x ficha
     * @return ArrayList que contiene las multiples fichas que puedan ser ubicadas en la celda dada
     * en caso de no encontra ninguna ficha valida, se devuelve el ArrayList vacio
     */
    public ArrayList posibles(Integer[][] tablero, ArrayList<int[]> bolsa, int fila, int columna){
        int cara1, cara2;
        ArrayList<int[]> posibles = new ArrayList<>();
        for(int i=0; i<bolsa.size(); i++){
            cara1 = bolsa.get(i)[0];
            cara2 = bolsa.get(i)[1];
            for(int rotacion=0; rotacion<271; rotacion+=90){
                if(safe(tablero, cara1,cara2,rotacion,fila,columna)){
                    int[] fichaValida = new int[3];
                    fichaValida[0] = cara1;
                    fichaValida[1] = cara2;
                    fichaValida[2] = rotacion;
                    posibles.add(fichaValida);
                }

            }
        }

        return posibles;
    }

    /**
     * Se verifica  que la ficha cumpla las 3 reglas del juego y
     * se pueda colocar en x celda, en una determinada rotacion
     * la validacion tiene en cuenta ambas caras, para que la ficha completa pueda ser ubicada sin ningun problema
     * además que ninguna de estas se salga del tablero de 9x9
     * @param tablero tablero del juego
     * @param cara1 cara1 de x ficha
     * @param cara2 caraw de x ficha
     * @param rotacion una de las 4 rotacion que podemos girar la ficha
     * @param fila fila a ubicar la ficha
     * @param columna columna a ubicar la columna
     * @return boolean
     */
    public boolean safe(Integer[][] tablero, int cara1, int cara2, int rotacion, int fila, int columna)
    {
    	boolean autorizada = false; 
        if(rotacion==0) 
        {
            if (fila + 1 > 8) 
            {
                autorizada = false;
            }
            else if (tablero[fila + 1][columna] == 0)
            {
                if ((validarFila(tablero, fila, cara1)) && (validarFila(tablero, fila + 1, cara2)) &&
                        (validarColumna(tablero, columna, cara1)) && (validarColumna(tablero, columna, cara2)) &&
                        (validarCaja(tablero, fila - (fila % 3), columna - (columna % 3), cara1)) &&
                        (validarCaja(tablero, (fila + 1) - ((fila + 1) % 3), columna - (columna % 3), cara2)))
                {
                    autorizada = true;
                }
            } 
            else 
            {
                autorizada = false;
            }
        }

        else if(rotacion==90)
        {
            if (columna + 1 > 8) 
            {
                autorizada = false;
            }
            else if (tablero[fila][columna + 1] == 0)
            {
                if ((validarFila(tablero, fila, cara1)) && (validarFila(tablero, fila, cara2)) &&
                        (validarColumna(tablero, columna + 1, cara1)) && (validarColumna(tablero,columna, cara2)) &&
                        (validarCaja(tablero,fila - (fila % 3), (columna + 1) - ((columna + 1) % 3), cara1)) &&
                        (validarCaja(tablero,fila - (fila % 3), columna - (columna % 3), cara2))) 
                {
                    autorizada = true;
                }
            } 
            else 
            {
                autorizada = false;
            }
        }

        else if(rotacion==180)
        {
            if(fila+1 > 8) 
            	{
            		autorizada = false;
            	}
           	else if(tablero[fila +1][columna] == 0)
           	{
                if( (validarFila(tablero, fila, cara2)) && (validarFila(tablero , fila+1, cara1)) &&
                        (validarColumna(tablero, columna, cara1)) && (validarColumna(tablero, columna, cara2)) &&
                        (validarCaja(tablero, (fila + 1) - ((fila + 1)%3), columna - (columna%3), cara1)) &&
                        (validarCaja(tablero, fila - (fila%3), columna - (columna%3), cara2)))
                {
                        autorizada = true;
                }
            }
            else
            	{
            		autorizada = false;
            	}
        }

        else //Es rotacion 270 
        {
            if (columna + 1 > 8) 
            {
                autorizada = false;
            }

            else if (tablero[fila][columna + 1] == 0)
            {
                if ((validarFila(tablero, fila, cara1)) && (validarFila(tablero, fila, cara2)) &&
                        (validarColumna(tablero, columna, cara1)) && (validarColumna(tablero,columna + 1, cara2)) &&
                        (validarCaja(tablero, fila - (fila % 3), columna - (columna % 3), cara1)) &&
                        (validarCaja(tablero, fila - (fila % 3), (columna + 1 - (columna + 1) % 3), cara2)))
                {
                    autorizada = true;
                }
            } 
            else 
            {
                autorizada = false;
            }
        }

        return autorizada;
    }

    /**
     * Se valida que la cara de la ficha no se encuentre repetida en x fila
     * @param tablero tablero del juego
     * @param fila columna a validar
     * @param cara caara de x ficha
     * @return boolean
     */
    public boolean validarFila(Integer[][] tablero, int fila, int cara) {
        for (int i = 0; i < 9; i++) {
            if (tablero[fila][i] == cara) {
                return false;
            }
        }
        return true;
    }

    /**
     * Se valida que la cara de la ficha no se encuentre repetida en x columna
     * @param tablero tablero del juego
     * @param columna columna a validar
     * @param cara caara de x ficha
     * @return boolean
     */
    public boolean validarColumna(Integer[][] tablero, int columna, int cara) {
        for (int i = 0; i < 9; i++) {
            if (tablero[i][columna] == cara) {
                return false;
            }
        }
        return true;
    }

    /**
     * Se valida que una cara de x ficha no se escuentre repetida en la region a insertar
     * @param tablero tablero del juego
     * @param filaCaja fila en la que inicia la "region" en la cual esta situada la cara de la ficha
     * @param columnaCaja columna  la que inicia la "region" en la cual esta situada la cara de la ficha
     * @param cara una de las dos caras de la ficha
     * @return boolean
     */
    public boolean validarCaja(Integer[][] tablero, int filaCaja, int columnaCaja, int cara) {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (tablero[i + filaCaja][j + columnaCaja] == cara) {
                    return false;
                }
            }
        }
        return true;
    }
}
