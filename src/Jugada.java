package src;
import java.util.ArrayList;

/**
 *
         * Jhon Edinson Blandon Quintero – 1255414
         * Sebastian Cano Ruiz – 1556156
         * Juan Manuel Sanchez Villada   – 1556275
         */

public class Jugada
{
    private Integer[][] tablero = new Integer[9][9];
    private ArrayList<int []> bolsa = new ArrayList<>();

    public Jugada(Integer[][] tablero, ArrayList<int[]> bolsa)
    {
        this.tablero = tablero;
        this.bolsa = bolsa;
    }

    public Integer[][] getTablero()
    {
        return tablero;
    }

    public ArrayList<int[]> getBolsa()
    {
        return bolsa;
    }
}
