package src;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;
/**
 *
 * Jhon Edinson Blandon Quintero – 1255414
 * Sebastian Cano Ruiz – 1556156
 * Juan Manuel Sanchez Villada   – 1556275
 */
public class ClasePrincipal{

    /**Este metodo se encarga de leer el archivo de texto .txt.
     * Toma los datos separados por espacios vacio y los va copiando en una matriz de enteros que representa el tablero
     * del sudominoku
     */
    public static void InicializarTablero(Integer [][] tablero) throws FileNotFoundException, IOException{
        String cadena;

        FileReader f = new FileReader("src/tableros/Tablero1.txt");
        BufferedReader b = new BufferedReader(f);
        int fila = 0;

        while((cadena = b.readLine())!=null) {
            String[] values = cadena.split(" ");
            for (int columna = 0; columna<values.length; columna++) {
                tablero[fila][columna] = Integer.valueOf(values[columna]);
            }
            fila ++;
        }

        b.close();

        /**
        JPanel mainPanel = null;
        JFileChooser fileChooser = new JFileChooser();
        int selection = fileChooser.showOpenDialog(mainPanel);

        if (selection == JFileChooser.APPROVE_OPTION)
        {

        }

        else
            JOptionPane.showMessageDialog(
                    null, "Documento Erroneo","IMPORTANTE",JOptionPane.ERROR_MESSAGE);
         */
    }

    /**
     * Muestra por consola el tablero del sudominoku
     * @param tablero Integer[][] matriz que representa el tablero del sudominoku
     */
    public static void mostrarTablero(Integer[][] tablero){
        System.out.println("Tablero Su-Domino-ku");
        System.out.print("    ");
        for(int i=0;i<9;i++){
            System.out.print((i+1)+" ");
        }
        System.out.println();
        System.out.print("    ");
        for(int i=0;i<9;i++){
            System.out.print("* ");
        }
        System.out.println();
        for(int i = 0; i<tablero.length ; i++){
            System.out.print((i+1)+" * ");
            for(int j = 0; j<tablero.length;j++){
                System.out.print(tablero[i][j]+"|");
            }
            System.out.println();
        }
        System.out.println();
    }

    /**
     * Inicializa la bolsa con las fichas para llenar el tablero, cada ficha es un arreglo ([cara1, cara2])
     * @param bolsa ArrayList<int []>
     */
    public static void llenarBolsa(ArrayList<int[]> bolsa)
    {
        for (int i = 1; i <9 ; i++) {
            for (int j = i+1; j <10 ; j++) {
                int[] ficha = {i, j};
                bolsa.add(ficha);
            }
        }
    }

    public static void main(String[] args) throws IOException
    {
        Double promedio = 0.0;
        int[] solutionVegas2;
        boolean solutionVegas1;
        int cant_true = 0;
        int cant_false = 0;
        long i = 1;
        Integer[][] tablero = new Integer[9][9];
        ArrayList<int[]> bolsa = new ArrayList<>();
        llenarBolsa(bolsa);
        InicializarTablero(tablero);
        mostrarTablero(tablero);
        SudominokuSolution sudominokuSolution = new SudominokuSolution();

        String entradaTeclado;
        Scanner entradaEscaner = new Scanner(System.in);
        System.out.println ("|---- Solucion Sudominoku ----|");
        System.out.println("1. Busquedad por Amplitud");
        System.out.println("2. Vegas Tipo 1");
        System.out.println("3. Vegas Tipo 2");
        entradaTeclado = entradaEscaner.nextLine ();

        if(Integer.parseInt(entradaTeclado) == 1)
        {
            solutionVegas1 = sudominokuSolution.algoritmoAmplitud_And_Vegas1(tablero, bolsa, false);
            System.out.println();
            mostrarTablero(tablero);
        }

        else if(Integer.parseInt(entradaTeclado) == 2)
        {
            solutionVegas1 = sudominokuSolution.algoritmoAmplitud_And_Vegas1(tablero, bolsa, true);
            System.out.println();
            mostrarTablero(tablero);
        }

        else
        {
            while (true)
            {
                System.out.println(i);
                solutionVegas2 = sudominokuSolution.algoritoVegas2(tablero, bolsa);
                if (solutionVegas2[0] == 0)
                {
                    cant_true ++;
                    mostrarTablero(tablero);
                    break;
                }
                else
                {
                    cant_false++;
                    promedio += solutionVegas2[1];
                }
                i++;
            }
            System.out.println();
            System.out.println("Total de iteraciones: " + i);
            System.out.println("Cantidad de veces que se encontro solucion: " + cant_true);
            System.out.println("Cantidad de veces que fallo: " + cant_false);
            System.out.println("Promedio de fichas ubicadas antes de fallar: " + promedio/cant_false);
        }
    }

}